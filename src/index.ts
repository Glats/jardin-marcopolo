import * as angular from 'angular';


import 'angular-ui-router';
import 'angular-ui-bootstrap';
import routesConfig from './routes';
import runBlock from './run';

import {main} from './app/main/main';
import {header} from './app/header/header';
import {footer} from './app/footer/footer';
import {home} from './app/home/home';
import {about} from './app/about/about';
import {navigation} from './app/navigation/navigation';
import {infrastructure} from './app/infrastructure/infrastructure';
import {contact} from './app/contact/contact';
import {history} from './app/history/history';
import {imageLoader} from './artifacts/components/images/loader';


import './index.scss';

angular
  .module('app', ['ui.router', 'ui.bootstrap'])
  .config(routesConfig)
  .run(runBlock)
  .component('main', main)
  .component('home', home)
  .component('about', about)
  .component('contact', contact)
  .component('history', history)
  .component('infrastructure', infrastructure)
  .component('marcoPoloNavigation', navigation)
  .component('marcoPoloHeader', header)
  .component('marcoPoloFooter', footer)
  .component('imageLoader', imageLoader)
;
