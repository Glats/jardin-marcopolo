import {TransitionService} from 'angular-ui-router';
import * as angular from 'angular';

/* @ngInject */
export default function runBlock($document: angular.IDocumentService, $transitions: TransitionService): void {
  let title = $document[0].title;
  $transitions.onSuccess({}, ($transition) => {
    if (angular.isDefined($transition.$to().data) && angular.isDefined($transition.$to().data.pageTitle)) {
      $document[0].title = $transition.$to().data.pageTitle + ' - ' + title;
    }
  });
}
