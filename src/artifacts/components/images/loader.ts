class ImageLoaderController {
  public url: string;

  /** @ngInject */
  constructor(private $state: angular.ui.IStateService) {
    this.url = $state.current.name;
  }

  range(min: number, max: number, step: number) {
    step = step || 1;
    let input = [];
    for (let i = min; i <= max; i += step) {
      input.push(i);
    }
    return input;
  }
}
export const imageLoader: angular.IComponentOptions = {
  template: require('./loader.html'),
  controller: ImageLoaderController
};
