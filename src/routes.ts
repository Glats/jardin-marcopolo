export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider, $locationProvider: angular.ILocationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('app', {
      url: '/',
      views: {
        'imageLoader': {component: 'imageLoader'},
        'content': {component: 'main'},
      },
      data : { pageTitle: 'Bienvenido' }
    })
    .state('app.home', {
      url: 'home',
      views: {
        'imageLoader@': {component: 'imageLoader'},
        'header@': {component: 'marcoPoloHeader'},
        'navigation@': {component: 'marcoPoloNavigation'},
        'content@': {component: 'home'},
        'footer@': {component: 'marcoPoloFooter'},
      },
      resolve: {
        displayFooter: () => {
          return true;
        }
      },
      data : { pageTitle: 'Home' }
    })

    .state('app.infrastructure', {
      url: 'infrastructure',
      views: {
        'imageLoader@': {component: 'imageLoader'},
        'header@': {component: 'marcoPoloHeader'},
        'navigation@': {component: 'marcoPoloNavigation'},
        'content@': {component: 'infrastructure'},
        'footer@': {component: 'marcoPoloFooter'},
      },
      data : { pageTitle: 'Infraestructura' }
    })
    .state('app.about', {
      url: 'about',
      views: {
        'imageLoader@': {component: 'imageLoader'},
        'header@': {component: 'marcoPoloHeader'},
        'navigation@': {component: 'marcoPoloNavigation'},
        'content@': {component: 'about'},
        'footer@': {component: 'marcoPoloFooter'},
      },
      data: { pageTitle: 'Qué ofrecemos'}
    })
    .state('app.contact', {
      url: 'contact',
      views: {
        'imageLoader@': {component: 'imageLoader'},
        'header@': {component: 'marcoPoloHeader'},
        'navigation@': {component: 'marcoPoloNavigation'},
        'content@': {component: 'contact'},
        'footer@': {component: 'marcoPoloFooter'},
      },
      data: { pageTitle: 'Contacto'}
    })
    .state('app.history', {
      url: 'history',
      views: {
        'imageLoader@': {component: 'imageLoader'},
        'header@': {component: 'marcoPoloHeader'},
        'navigation@': {component: 'marcoPoloNavigation'},
        'content@': {component: 'history'},
        'footer@': {component: 'marcoPoloFooter'},
      },
      data: { pageTitle: 'Historia'}
    })
  ;
}
