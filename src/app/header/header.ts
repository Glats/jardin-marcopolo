class HeaderController {
  public url: string;

  /** @ngInject */
  constructor(private $state: angular.ui.IStateService) {
    this.url = $state.current.name;
  }
}

export const header: angular.IComponentOptions = {
  template: require('./header.html'),
  controller: HeaderController,
  controllerAs: 'headerCtrl'
};
