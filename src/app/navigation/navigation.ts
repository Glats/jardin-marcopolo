class NavigationController {
  public url: string;

  /** @ngInject */
  constructor(private $state: angular.ui.IStateService) {
    this.url = $state.current.name;
  }
}

export const navigation: angular.IComponentOptions = {
  template: require('./navigation.html'),
  controller: NavigationController,
  controllerAs: 'navCtrl'
};
