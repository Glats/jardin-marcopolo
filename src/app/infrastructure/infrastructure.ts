import {Image} from './image';

class InfrastructureController {
  public active: number;
  public noWrapSlides: boolean;
  public slides: Image[] = [];
  public interval: number;

  /** @Inject */
  constructor() {
    this.active = 0;
    this.noWrapSlides = false;
    this.interval = 5000;
    for (let i = 1; i < 5; i++) {
      this.slides.push(new Image(
        i, '../../assets/img/photos/place' + i + '.jpg',
      ));
    }
  }


}

export const infrastructure: angular.IComponentOptions = {
  template: require('./infrastructure.html'),
  controller: InfrastructureController,
  controllerAs: 'infraCtrl'
};
