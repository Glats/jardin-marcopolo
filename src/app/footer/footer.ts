class FooterController {

  public url: string;

  /** @ngInject */
  constructor(private $state: angular.ui.IStateService) {
    this.url = $state.current.name;
  }
}

export const footer: angular.IComponentOptions = {
  template: require('./footer.html'),
  controller: FooterController,
  controllerAs: 'footerCtrl',
  bindings: {displayFooter: '<'}
};
